import java.util.Scanner;

public class Dice {


    public static void main(String[] args) {
        int die1;   // The number on the first die.
        int die2;   // The number on the second die.
        int roll;   // The total roll (sum of the two dice).
        String result1 = null;
        String result2 = null;


        die1 = (int) (Math.random() * 6) + 1;
        die2 = (int) (Math.random() * 6) + 1;
        roll = die1 + die2;


        switch (die1) {
            case 1:
                result1 = "+-----+ " + "\n" + "|     | " + "\n" + "|  *  | " + "\n" + "|     | " + "\n" + "+-----+ ";
                break;
            case 2:
                result1 = "+-----+" + "\n" + "| *   |" + "\n" + "|     |" + "\n" + "|   * |" + "\n" + "+-----+";
                break;
            case 3:
                result1 = "+-----+" + "\n" + "| *   |" + "\n" + "|  *  |" + "\n" + "|   * |" + "\n" + "+-----+";
                break;
            case 4:
                result1 = "+-----+" + "\n" + "|*   *|" + "\n" + "|     |" + "\n" + "|*   *|" + "\n" + "+-----+";
                break;
            case 5:
                result1 = "+-----+" + "\n" + "|*   *|" + "\n" + "|  *  |" + "\n" + "|*   *|" + "\n" + "+-----+";
                break;
            case 6:
                result1 = "+-----+" + "\n" + "|* * *|" + "\n" + "|* * *|" + "\n" + "|* * *|" + "\n" + "+-----+";
                break;
        }

        switch (die2) {
            case 1:
                result2 = "+-----+" + "\n" + "|     |" + "\n" + "|  *  |" + "\n" + "|     |" + "\n" + "+-----+";
                break;
            case 2:
                result2 = "+-----+" + "\n" + "| *   |" + "\n" + "|     |" + "\n" + "|   * |" + "\n" + "+-----+";
                break;
            case 3:
                result2 = "+-----+" + "\n" + "| *   |" + "\n" + "|  *  |" + "\n" + "|   * |" + "\n" + "+-----+";
                break;
            case 4:
                result2 = "+-----+" + "\n" + "|*   *|" + "\n" + "|     |" + "\n" + "|*   *|" + "\n" + "+-----+";
                break;
            case 5:
                result2 = "+-----+" + "\n" + "|*   *|" + "\n" + "|  *  |" + "\n" + "|*   *|" + "\n" + "+-----+";
                break;
            case 6:
                result2 = "+-----+" + "\n" + "|* * *|" + "\n" + "|* * *|" + "\n" + "|* * *|" + "\n" + "+-----+";
                break;
        }

        String resultaat3 = String.format("%s \n%s = %d", result1, result2, roll);
        System.out.println(resultaat3);


        }



    }

}
